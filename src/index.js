import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function Products() {
    return (
        <table>
            <h1>Знаки зодіака</h1>
            <div className="table">
                <thead>
                    <tr className="header">
                        <th>Назва знаку зодіака</th>
                        <th>Період дії</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="list">
                        <td>Овен</td>                        
                        <td>Телець</td>                       
                        <td>Близнюки</td>                        
                        <td>Рак</td>                        
                        <td>Лев</td>                       
                        <td>Діва</td>                        
                        <td>Терези</td>                        
                        <td>Скорпіон</td>                        
                        <td>Стрілець</td>                       
                        <td>Козоріг</td>                        
                        <td>Водолій</td>                        
                        <td>Риби</td>                        
                    </tr>
                    <tr className="list">
                    <td>21.03-19.04</td>
                    <td>20.04-20.05</td>
                    <td>21.05-20.06</td>
                    <td>21.06-22.07</td>
                    <td>23.07-22.08</td>
                    <td>23.08-22.09</td>
                    <td>23.09-22.10</td>
                    <td>23.10-21.11</td>
                    <td>22.11-21.12</td>
                    <td>22.12-19.01</td>
                    <td>20.01-18.02</td>
                    <td>19.02-20.03</td>
                    </tr>
                </tbody>
            </div>

        </table>
    )
}

ReactDOM.render(<Products></Products>, document.querySelector(".zodiac"));